from urllib import parse
from urllib import request
from bs4 import BeautifulSoup
import configparser
import os


SETTING_FILE = 'lib/setting.ini'
inifile = configparser.ConfigParser()
inifile.read(SETTING_FILE)

AZLYRICS = inifile['url']['azlyrics']
JLYRICS = inifile['url']['jlyrics']
SAVE_PATH = inifile['save']['lyrics']
IMAS_PATH = inifile['save']['im@s']


def _getLyricsByAZlyrics(url):
    html = request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    or_lyric = soup.find('div', class_='', id='')
    or_lyric = or_lyric.text

    lyric = or_lyric.replace('<br/>', '')

    return lyric


def getMusicListByAZlyrics(title='', artist=''):
    title = parse.quote_plus(title)
    artist = parse.quote_plus(artist)

    url = AZLYRICS + 'search.php?q={}'.format(artist + ' ' + title)

    html = request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    search_list = soup.find_all("td", class_="text-left visitedlyr")

    title_array = []
    artist_array = []
    url_array = []
    music_list = []
    for music in search_list :
        tmp = music.find_all('b')
        tmp_lyrics = music.find('a')

        if len(tmp) < 2 :
            continue

        title_array.append(tmp[0].text)
        artist_array.append(tmp[1].text)
        url_array.append(tmp_lyrics['href'])

    for x in range(len(title_array)) :
        tmp_music = {}
        tmp_music['title'] = title_array[x]
        tmp_music['artist'] = artist_array[x]
        tmp_music['url'] = url_array[x]

        music_list.append(tmp_music)

    return music_list


def _getLyricsByJlyrics(url):
    html = request.urlopen(url)
    soup = BeautifulSoup(html, "html.parser")

    or_lyric = soup.find('p', id='lyricBody')
    or_lyric = or_lyric.text

    # 整形
    lyric = or_lyric.replace('<br/>', '')

    return lyric


def getMusicListByJlyrics(title='', artist=''):

    title = parse.quote_plus(title)
    artist = parse.quote_plus(artist)

    url = JLYRICS + 'index.php?kt={0}&ct=2&ka={1}&ca=2&kl=&cl=0'.format(title, artist)

    html = request.urlopen(url)

    soup = BeautifulSoup(html, 'html.parser')
    lylicList = soup.find_all('div', id='lyricList')

    title_array = []
    artist_array = []
    url_array = []
    music_list = []
    for x in lylicList :
        for music in x.find_all('div', class_='title'):
            tmp = music.find('a')
            title_array.append(tmp.text)
            url_array.append(JLYRICS + tmp.attrs['href'])

        for music in x.find_all('div', class_='status'):
            tmp = music.find('a')
            artist_array.append(tmp.text)

    for x in range(len(title_array)):
        tmp_music = {}
        tmp_music['title'] = title_array[x]
        tmp_music['artist'] = artist_array[x]
        tmp_music['url'] = url_array[x]

        music_list.append(tmp_music)

    return music_list


def saveLyric(title, artist='other', lyric=''):
    filepath = SAVE_PATH + repr(artist)[1:-1]
    if not os.path.isdir(filepath):
        os.mkdir(filepath)

    filepath += '/'
    filename = repr(title)[1:-1] + '.txt'
    if not os.path.isfile(filepath + filename):
        with open(filepath + filename, 'wt') as f:
            f.write(lyric)
            return True

    return False


def getLyricBydir(title, artist):
    lyric_path = SAVE_PATH + repr(artist)[1:-1] + '/' + repr(title)[1:-1] + '.txt'
    if os.path.isfile(lyric_path):
        with open(lyric_path, 'rt') as f:
            lyric = f.read()
            lyric = lyric.replace('\n\n', '<NPOB-REP>').replace('　', ' ').replace('\n', '  ')
            lyric = lyric.replace('<NPOB-REP>', '\n\n')

            return lyric

    return False


def getLyricByIMAS(title):
    lyric_path = IMAS_PATH + repr(title)[1:-1] + '.txt'
    print(lyric_path)
    if os.path.isfile(lyric_path):
        with open(lyric_path, 'rt') as f:
            lyric = f.read()
            lyric = lyric.replace('\n\n', '<NPOB-REP>').replace('　', ' ').replace('\n', '  ')
            lyric = lyric.replace('<NPOB-REP>', '\n\n')

            return lyric

    return False


if __name__ =='__main__':
    title = '恋'
    artist = '星野源'
    music_list = getMusicListByJlyrics(title, artist)
    lyric = _getLyricsByJlyrics(music_list[0]['url'])
    print(lyric)
