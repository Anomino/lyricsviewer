from subprocess import check_output, CalledProcessError
import re
from taglib import File


def getPID(process_name):
    try:
        return int(check_output(['pidof', '-s', process_name]))

    except CalledProcessError:
        return None


def getPlayedMusic(pid, musics_path):
    try:
        res = check_output(['lsof', '-p', str(pid)]).decode('utf-8')

    except CalledProcessError:
        return None

    search = '{0}.*\n'.format(musics_path)
    match = re.findall(search, res)

    if match:
        return match[0].replace('\n', '')

    return None


def getMusicsInfo(filepath):
    music = File(filepath)
    return music.tags


if __name__ == '__main__':
    music_path = r'/home/anomino/Musics/'
    pid = getPID('sayonara')

    if pid is None:
        print('such process is not existed')
        exit(1)

    match = getPlayedMusic(pid, music_path)
    info = getMusicsInfo(match)
    print('title : {0}     artist : {1}     arbum : {2}'.format(info['TITLE'][0], info['ARTIST'][0]), info['ALBUM'][0])
