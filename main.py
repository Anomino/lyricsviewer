from lib import getInfoByPID
from lib import getInfoByInternet
from subprocess import call
import signal
import time
import configparser
import re

inifile = configparser.ConfigParser()
inifile.read('lib/setting.ini')
MUSIC_PATH = inifile['save']['musics']


class LyricsViewer:
    def __init__(self, music_path=MUSIC_PATH, process_name='sayonara'):
        self.current_music = ''
        self.last_music = ''
        self.music_path = music_path
        self.lyric = ''
        self.process_name = process_name


    def monitor(self):
        pid = getInfoByPID.getPID(self.process_name)
        music_path = getInfoByPID.getPlayedMusic(pid, self.music_path)
        music_info = getInfoByPID.getMusicsInfo(music_path)

        spl_str = re.split('\(', music_info['TITLE'][0])
        tmp_str = ''

        for x in spl_str:
            if ')' in x:
                x = re.sub('.*\)', '', x)

            tmp_str += x

        music_info['TITLE'][0] = tmp_str

        spl_str = re.split('\(', music_info['ARTIST'][0])
        tmp_str = ''

        for x in spl_str:
            if ')' in x:
                x = re.sub('.*\)', '', x)

            tmp_str += x

        music_info['ARTIST'][0] = tmp_str

        # first time
        if not self.current_music:
            self.current_music = music_info

        elif not self.current_music['TITLE'][0] == music_info['TITLE'][0] and\
            not self.current_music['ARTIST'][0] == music_info['ARTIST'][0]:
            self.last_music = self.current_music
            self.current_music = music_info

        else:
            return False

        return True

    def getLyric(self):
        lyric = getInfoByInternet.getLyricByIMAS(self.current_music['TITLE'][0])
        if lyric:
            res = input('Is this music THE IDOLM@STER?   y/n : ')
            if res == 'y':
                self.lyric = lyric
                return True


        print('search {0} {1}'.format(self.current_music['TITLE'][0], self.current_music['ARTIST'][0]))
        jlyric_list = getInfoByInternet.getMusicListByJlyrics(self.current_music['TITLE'][0],\
                                                              self.current_music['ARTIST'][0])

        for num, music in enumerate(jlyric_list):
                print('{0} : {1} by {2}'.format(num + 1, music['title'],\
                                                  music['artist'][0]))

        res = input('Which following musics are you listening to?')

        if res:
            index = int(res) - 1
            self.lyric = getInfoByInternet._getLyricsByJlyrics(jlyric_list[index])

            return True

        azlyric_list = getInfoByInternet.getMusicListByAZlyrics(self.current_music['TITLE'][0],\
                                                                self.current_music['ARTIST'][0])
        for num, music in enumerate(azlyric_list):
                print('{0} : {1} by {2}'.format(num + 1, music['title'],\
                                                  music['artist']))

        res = input('Which following musics are you listening to?')

        if res:
            index = int(res) - 1
            self.lyric = getInfoByInternet._getLyricsByJlyrics(jlyric_list[index])

            return True

        return False

    def showLyric(self):
        call(['clear'])
        print('-----------------------------------------------------------------------\n' +
              '{0}\n{1}\n{2}\n'.format(self.current_music['TITLE'][0], self.current_music['ARTIST'][0],\
                                       self.current_music['ALBUM'][0]) +
              '-----------------------------------------------------------------------\n' +
              self.lyric)

    def run(self):
        if self.monitor():
            if self.getLyric():
                self.showLyric()


if __name__ == '__main__':
    instance = LyricsViewer(MUSIC_PATH, 'sayonara')

    while True:
        try:
            time.sleep(3)
            instance.run()
        except KeyboardInterrupt:
            print('Terminate')
            exit(0)
